﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework; 
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace VirtualDrone
{
    public enum DroneType
    {
        Automated,
        SemiAutomated
    }

    public class Drone
    {
        Texture2D texture;
        Vector2 position;
        Vector2 speed;
        Rectangle bounds;
        DroneType type;
        public List<Drone> RangeRegister;
        public Guid DroneID;
        public int Size;
        public int CollisionBuffer;

        public Drone(Texture2D texture, Vector2 position, Vector2 speed, Rectangle bounds, DroneType type = DroneType.Automated)
        {
            this.texture = texture;
            this.position = position;
            this.speed = speed;
            this.bounds = bounds;
            this.type = type;
            this.RangeRegister = new List<Drone>();
            this.DroneID = Guid.NewGuid();
            this.Size = (texture.Width / 2);
            this.CollisionBuffer = 10;
        }

        public void Draw(SpriteBatch classSprite, ContentManager contentManager) //classSprite will get the spritebatch variable passed to this Draw constructor
         {
             if (type == DroneType.Automated)
             {
                 classSprite.Draw(texture, position, Color.Red); //function to draw out the sprite
             }
             else
             {
                 classSprite.Draw(texture, position, Color.White);
             }
             classSprite.DrawString(contentManager.Load<SpriteFont>("Font/Arial"), "X " + position.X.ToString(), new Vector2(position.X,position.Y + 5), Color.Black);
             classSprite.DrawString(contentManager.Load<SpriteFont>("Font/Arial"), "Y " + position.Y.ToString(), new Vector2(position.X, position.Y + 15), Color.Black);
         }

        public void Update(GameTime gameTime)
        {
            DroneMotion(gameTime);
        }

        public void DroneMotion(GameTime gameTime) //gameTime keeps the game (and all its classes) in sync
        {
            int MaxX = bounds.Width - texture.Width;
            int MinX = bounds.X;
            int MaxY = bounds.Height - texture.Height;
            int MinY = bounds.Y;

            if (type == DroneType.SemiAutomated)
            {
                if (!Keyboard.GetState().IsKeyDown(Keys.Up) 
                    || !Keyboard.GetState().IsKeyDown(Keys.Down)
                    || !Keyboard.GetState().IsKeyDown(Keys.Left)
                    || !Keyboard.GetState().IsKeyDown(Keys.Right))
                {
                    position += speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                // Move the sprite around; update positions by arrow key presses
                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                {
                    //speed.Y *= 1;
                    position.Y -= 1;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    //speed.Y *= -1;
                    position.Y += 1;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    //speed.X *= -1;
                    position.X -= 1;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Right))
                {
                    //speed.X *= 1;
                    position.X += 1;
                }
            }
            else
            {
                position += speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }


            // Move the sprite by speed, scaled by elapsed time.
            foreach (Drone drone in RangeRegister)
            {
                if (DroneID != drone.DroneID)
                {
                    float xyDistance = Vector2.Distance(this.position, drone.position);
                    //avoid collision within 10px
                    if (xyDistance <= Size + CollisionBuffer)
                    {
                        speed.X *= -1;//oposite x direction
                        if (position.X >= (drone.X + (drone.Size + drone.CollisionBuffer)))
                        {
                            //speed.X *= -1;
                            position.X = (drone.X + (drone.Size + drone.CollisionBuffer +5));
                        }
                        else if (position.X <= (drone.X - (drone.Size + drone.CollisionBuffer)))
                        {
                            //speed.X *= -1;
                            position.X = (drone.X - (drone.Size + drone.CollisionBuffer + 5));
                        }

                        if (position.Y >= (drone.Y + (drone.Size + drone.CollisionBuffer)))
                        {
                            //speed.Y *= -1;
                            position.Y = (drone.Y + (drone.Size + drone.CollisionBuffer + 5));
                        }
                        else if (position.Y <= (drone.Y - (drone.Size + drone.CollisionBuffer)))
                        {
                            //speed.Y *= -1;
                            position.Y = (drone.Y - (drone.Size + drone.CollisionBuffer + 5));
                        }
                        
                    }
                }
            }

            // Check for boundary.
            if (position.X > MaxX)
            {
                speed.X *= -1;
                position.X = MaxX;
            }
            else if (position.X < MinX)
            {
                speed.X *= -1;
                position.X = MinX;
            }

            if (position.Y > MaxY)
            {
                speed.Y *= -1;
                position.Y = MaxY;
            }
            else if (position.Y < MinY)
            {
                speed.Y *= -1;
                position.Y = MinY;
            }
            
        }

        public Vector2 Postion
        {
            get { return position; }
        }

        public float X
        {
            get { return position.X; }
        }

        public float Y
        {
            get { return position.Y; }
        }

        public int Width
        {
            get { return texture.Width; }
        }

        public float Height
        {
            get { return texture.Height; }
        }
    }
}
