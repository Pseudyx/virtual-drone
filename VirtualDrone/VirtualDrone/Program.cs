using System;

namespace VirtualDrone
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (VirtualDrone game = new VirtualDrone())
            {
                game.Run();
            }
        }
    }
#endif
}

