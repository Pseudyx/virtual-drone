using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace VirtualDrone
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class VirtualDrone : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont Arial;

        string Distance, Negate, Min, Max, Length;
        
        Rectangle Boundary;
        public static ProximitySensor quad;
        //Drone drone;
        List<Drone> allDrones;

        public VirtualDrone()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Boundary = new Rectangle(0, 0, graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height);
            quad = new ProximitySensor(0, Boundary);
            allDrones = new List<Drone>();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Arial = Content.Load<SpriteFont>("Font/Arial");

            Drone drone = new Drone(Content.Load<Texture2D>("Sprite/Dot"), Vector2.Zero, new Vector2(50.0f, 50.0f), Boundary, DroneType.SemiAutomated);
            allDrones.Add(drone);

            Random rnd = new Random();
            for (int x = 0; x < 9; x++)
            {
                
                float fx = (float)rnd.Next(0, Boundary.Width);
                float fy = (float)rnd.Next(0, Boundary.Height);
                float sx = (float)rnd.Next(-50, 50);
                float sy = (float)rnd.Next(-50, 50);
                Drone autoDrone = new Drone(Content.Load<Texture2D>("Sprite/Dot"), new Vector2(fx, fy), new Vector2(sx, sy), Boundary);
                allDrones.Add(autoDrone);
            }
           

        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            //xyDistance = Vector2.Distance(drone.Postion, autoDrone.Postion);
            //Distance = xyDistance.ToString();
            //Negate = Vector2.Negate(drone.Postion).ToString();
            //Min = Vector2.Min(drone.Postion, autoDrone.Postion).ToString();
            //Max = Vector2.Max(drone.Postion, autoDrone.Postion).ToString();
            //Length = drone.Postion.Length().ToString();
          
            quad.clear();
            foreach (Drone drone in allDrones)
            {
                quad.insert(drone);
            }

            List<Drone> returnObjects = new List<Drone>();
            for (int i = 0; i < allDrones.Count - 1; i++)
            {
                returnObjects.Clear();
                quad.retrieve(returnObjects, quad.objs[i]);

                for (int x = 0; x < returnObjects.Count; x++)
                {
                    // Run collision detection algorithm between objects
                    returnObjects[i].RangeRegister = returnObjects;
                }

                allDrones[i].Update(gameTime);
            }

            base.Update(gameTime);
        }
        
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            //spriteBatch.DrawString(Arial, "Distance: " + Distance, new Vector2(10, 10), Color.Black);
            //spriteBatch.DrawString(Arial, "Negate: " + Negate, new Vector2(10, 30), Color.Black);
            //spriteBatch.DrawString(Arial, "Min: " + Min, new Vector2(10, 50), Color.Black);
            //spriteBatch.DrawString(Arial, "Max: " + Max, new Vector2(10, 70), Color.Black);
            //spriteBatch.DrawString(Arial, "Length: " + Length, new Vector2(10, 90), Color.Black);

            foreach (Drone drone in allDrones)
            {
                drone.Draw(spriteBatch, Content);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
