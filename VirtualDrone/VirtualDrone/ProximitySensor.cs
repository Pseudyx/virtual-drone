﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework; 
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace VirtualDrone
{
    //Proximity Sensor is a standard Quadtree grid system 
    //for determining which objects need collision control
    //with respect tp each object.
    public class ProximitySensor
    {
        private int MaxObj = 100;
        private int MaxLvl = 5;

        private int lvl;
        private Rectangle bounds;
        private ProximitySensor[] nodes;
        public List<Drone> objs;

        public ProximitySensor(int Level, Rectangle Bounds)
        {
            this.lvl = Level;
            this.bounds = Bounds;
            objs = new List<Drone>();
            nodes = new ProximitySensor[4];
        }

        public void clear()
        {
            objs.Clear();

            for (int i = 0; i < nodes.Length; i++)
            {
                if (nodes[i] != null)
                {
                    nodes[i].clear();
                    nodes[i] = null;
                }
            }
        }

        private void split()
        {
            int subWidth = (int)(bounds.Width / 2);
            int subHeight = (int)(bounds.Height / 2);
            int x = (int)bounds.X;
            int y = (int)bounds.Y;

            nodes[0] = new ProximitySensor(lvl + 1, new Rectangle(x + subWidth, y, subWidth, subHeight));
            nodes[1] = new ProximitySensor(lvl + 1, new Rectangle(x, y, subWidth, subHeight));
            nodes[2] = new ProximitySensor(lvl + 1, new Rectangle(x, y + subHeight, subWidth, subHeight));
            nodes[3] = new ProximitySensor(lvl + 1, new Rectangle(x + subWidth, y + subHeight, subWidth, subHeight));
        }

        private int getIndex(Drone drone)
        {
            int index = -1;
            double verticalMidpoint = bounds.X + (bounds.Width / 2);
            double horizontalMidpoint = bounds.Y + (bounds.Height / 2);

            // Object can completely fit within the top quadrants
            bool topQuadrant = (drone.Y < horizontalMidpoint && drone.Y + drone.Height < horizontalMidpoint);
            // Object can completely fit within the bottom quadrants
            bool bottomQuadrant = (drone.Y > horizontalMidpoint);

            // Object can completely fit within the left quadrants
            if (drone.X < verticalMidpoint && drone.X + drone.Width < verticalMidpoint)
            {
                if (topQuadrant)
                {
                    index = 1;
                }
                else if (bottomQuadrant)
                {
                    index = 2;
                }
            }
            // Object can completely fit within the right quadrants
            else if (drone.X > verticalMidpoint)
            {
                if (topQuadrant)
                {
                    index = 0;
                }
                else if (bottomQuadrant)
                {
                    index = 3;
                }
            }

            return index;
        }

        public void insert(Drone drone)
        {
            if (nodes[0] != null)
            {
                int index = getIndex(drone);

                if (index != -1)
                {
                    nodes[index].insert(drone);

                    return;
                }
            }

            objs.Add(drone);

            if (objs.Count > MaxObj && lvl < MaxLvl)
            {
                if (nodes[0] == null)
                {
                    split();
                }

                int i = 0;
                while (i < objs.Count)
                {
                    int index = getIndex(objs[i]);
                    if (index != -1)
                    {
                        nodes[index].insert(objs[i]);
                        objs.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        public List<Drone> retrieve(List<Drone> returnObjects, Drone drone)
        {
            int index = getIndex(drone);
            if (index != -1 && nodes[0] != null)
            {
                nodes[index].retrieve(returnObjects, drone);
            }

            returnObjects.AddRange(objs);

            return returnObjects;
        }

    }
}
